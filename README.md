# ~/ABRÜPT/SIMONE WEIL/L'ESPAGNOLE/*

La [page de ce livre](https://abrupt.ch/simone-weil/lespagnole/) sur le réseau.

## Sur le livre

Ce recueil de textes de Simone Weil, écrits entre 1936 et 1938, témoigne de son expérience de la guerre d’Espagne. Simone Weil, l’Espagnole, n’a pas hésité à se rendre à Barcelone pour soutenir, au risque de sa vie, la cause d’un peuple pour lequel elle avait une affection sincère. L’Espagne fut la terre qui vit s’affirmer la force de caractère d’une femme prête à mourir pour ses convictions, elle fut malheureusement aussi le lieu où Simone Weil découvrit les affres de la guerre civile, les dérives des mouvements révolutionnaires, la médiocrité des hommes lorsqu’ils se confrontent au pouvoir. Dans le journal de son expérience du front, au-delà de son caractère historique, un style lapidaire laisse entendre une littérature qui se place sur la brèche. Les textes qui suivent le *Journal d’Espagne* donnent quant à eux un certain écho à la désillusion de Simone Weil tant face à la lâcheté politique qu’à l’inhumanité de ses camarades. Ce recueil se termine sur un article plus théorique, *Ne recommençons pas la guerre de Troie*, qui souligne le danger de l’utilisation de termes brumeux, d’entités vides : démocratie, capitalisme, communisme, liberté, etc. Les mots n’ont pas de contenu lorsqu’ils fourvoient les peuples et les précipitent dans le sang et dans la haine. Elle rejette catégoriquement les discours abstraits qui mettent en ordre de marche. Simone Weil y oppose une volonté de déconstruire les mécaniques du pouvoir et de son corollaire, le prestige du pouvoir. En ces textes, s’exprime toute la singularité de son humanité, faite d’intransigeance et de cohérence, de raison et de nuances, qui se place invariablement du côté des classes opprimées.

Ce recueil contient les textes :

- Journal d’Espagne
- Fragment de 1936 (« Que se passe-t-il en Espagne ? »)
- Lettre à Georges Bernanos
- Réflexions pour déplaire
- Faut-il graisser les godillots ?
- La politique de neutralité et l’assistance mutuelle
- Non-intervention généralisée
- Ne recommençons pas la guerre de Troie

## Sur l'autrice

1909, naissance, 1943, mort. Une vie brève mais qui se jeta intensément dans la question sociale du siècle. Simone Weil fait partie de ces rares penseurs qui accordent leur existence à leur pensée. Anarcho-syndicaliste et critique éclairée de Marx, christique et non chrétienne, de tous les combats pour défendre les sans-voix, malgré une érudition rare qui la mena de la mathématique au sanskrit, et qui aurait pu lui offrir la vie paisible du professorat, cette « martienne », selon le mot d’Alain, avait la merveilleuse intransigeance de la cohérence. Elle se rendit comme ouvrière à l’usine de 1934 à 1935, en dépit de sa santé fragile, puis en 1936, elle partit en Espagne pour soutenir ses camarades républicains, ne put accepter leurs écarts, continua à lutter, à écrire, développa une mystique de la justice et de la souffrance de l’absence de justice. Durant la guerre, elle se rendit à Londres pour rejoindre la France libre, ne pouvant supporter le confort américain. Elle voulut rentrer en France en tant que résistante, on le lui refusa. Tuberculeuse, à l’orée de la mort, par solidarité avec ses compatriotes subissant les restrictions de l’occupation, elle ne s’alimenta quasiment plus, et jusqu’à son dernier souffle, Simone Weil demeura fidèle à ce qu’elle défendait.

## Sur la licence

Cet [antilivre](https://abrupt.ch/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).
